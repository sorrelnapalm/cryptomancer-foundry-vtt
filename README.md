A community-made Foundry VTT system for Cryptomancer. Very much in pre-alpha right now. More details incoming.

Currently only supports Cryptomancer v1.5.

Many thanks to asacolips for their [Boilerplate System](https://gitlab.com/asacolips-projects/foundry-mods/boilerplate/) and [excellent tutorial](https://foundryvtt.wiki/en/development/guides/SD-tutorial)!

Made with permission from Land of NOP.

