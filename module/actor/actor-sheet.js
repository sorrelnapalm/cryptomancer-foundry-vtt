
/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class CryptomancerActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["cryptomancer", "sheet", "actor"],
      template: "systems/cryptomancer/templates/actor/actor-sheet.html",
      width: 600,
      height: 600,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "attributes" }]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];

    // Prepare items.
    if (this.actor.data.type == 'character') {
      this._prepareCharacterItems(data);
    }

    return data;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    // Initialize containers.
    const gear = [];
    const talents = [];
    const spells = {
      "Cantrip": [],
      "Basic Spell": [],
      "Greater Spell": []
    };

    // Iterate through items, allocating to containers
    for (let i of sheetData.items) {
      let item = i.data;
      i.img = i.img || DEFAULT_TOKEN;
      // Append to gear.
      if (i.type === 'item') {
        gear.push(i);
      }
      // Append to talents.
      else if (i.type === 'talent') {
        talents.push(i);
      }
      // Append to spells.
      else if (i.type === 'spell') {
        if (Object.keys(spells).includes(i.data.level)) {
          spells[i.data.level].push(i);
        }
      }
    }

    // Assign and return
    actorData.gear = gear;
    actorData.talents = talents;
    actorData.spells = spells;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Rollable attributes.
    html.find('.rollable').click(this._onRoll.bind(this));

    // Drag events for macros.
    if (this.actor.owner) {
      let handler = ev => this._onDragStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    const core = dataset.core;
    const attribute = dataset.attribute;
    const skill = dataset.skill;

    // handle rolling a skill
    if (attribute) {
      const actor = this.object.data;
      const actorData = actor.data;
      const value = actorData.core[core].attributes[attribute].value;

      // Display input for threshold and skill break/push
      let d = new Dialog({
        title: `Rolling ${skill}`,
        content: `
          <form class="flexcol">
            <div class="form-group">
              <label for="thresholdSelect">Threshold Select</label>
              <select name="thresholdSelect">
                <option value="4">4</option>
                <option value="6">6</option>
                <option value="8">8</option>
              </select>
            </div>
            <div class="form-group">
              <label for="skillBreakInput">Skill Break</label>
              <input type="checkbox" name="skillBreakInput" id="skillBreakInput"/>
            </div>
            <div class="form-group">
              <label for="skillPushInput">Skill Push</label>
              <input type="checkbox" name="skillPushInput" id="skillPushInput"/>
            </div>
          </form>
        `,
        buttons: {
          no: {
            icon: '<i class="fas fa-times"></i>',
            label: 'Cancel'
          },
          yes: {
            icon: '<i class="fas fa-check"></i>',
            label: 'Confirm',
            callback: (html) => {
              let threshold = html.find('[name="thresholdSelect"]').val();
              let skillBreak = html.find('[name="skillBreakInput"]')[0].checked;
              let skillPush = html.find('[name="skillPushInput"]')[0].checked;

              console.log(attribute, value, threshold, skillBreak, skillPush);

              // build roll and count hits, botches, etc.
              const skillDiceMods = [`cs>=${threshold}`, "df=1"];
              const fateDiceMods = ["cs=6", "df=1"];
              console.log(skillDiceMods, fateDiceMods);

              let roll = new Roll(`@core.${core}.attributes.${attribute}.value d10cs>=${threshold}df=1 + @core.${core}.attributes.${attribute}.fate d6cs=6df=1`, actorData);

              const before = roll.evaluate();

              let j = roll.toJSON();

              /** apply skill push and break by modifying the JSON object of
               * the roll
               */
              let skillBreakApplied = false;
              let total = 0;
              for (let t of j.terms) {

                // some terms are strings like "+"; skip them
                if (!(t instanceof DiceTerm)) continue;

                for (let r of t.results) {
                  if (r.success) {
                    if (skillPush && r.result === 10) {
                      r.count = 2;
                    }
                  } else if (r.failure) {
                    if (skillBreak && !skillBreakApplied) {
                      r.count = 1;
                      skillBreakApplied = true; // only turn one botch into hit
                    }
                  }
                }

                console.log(t.total);
                total += t.total;
              }

              j.total = total;
              j._total = total;

              /** create a new roll from the modified JSON as we can't
               * modify the terms' totals and the roll's total directly
               */
              roll = Roll.fromJSON(JSON.stringify(j));

              /** now we can use the roll's render function to print it
               * as a message and it'll display the correct totals taking into
               * account skill break and push
               */
              let label = `Rolling ${skill}:`;
              if (skillBreak) label += "<br>Skill Break: ✓"
              if (skillPush) label += "<br>Skill Push: ✓"

              roll.toMessage({
                speaker: ChatMessage.getSpeaker({actor: this.actor}),
                flavor: label
              });
            }
          }
        },
      }).render(true);
    }
  }

}
