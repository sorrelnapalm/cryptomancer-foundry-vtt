/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class CryptomancerActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags;

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === 'character') this._prepareCharacterData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;

    // Make modifications to data here. For example:

    // Calculate number of fate dice for each attribute
    for (let [k, c] of Object.entries(data.core)) {
      for (let [j, a] of Object.entries(c.attributes)) {
        if (1 <= a.value <= 5) {
          a.fate = 5 - a.value;
        } else {
          a.fate = 0;
        }
      }
    }

    /**
     * Re-calculate spent coin and UPs
     * Spent coin: tally up all coin costs for items
     * Spent UPs: tally up all UP costs for talents and spells
     *
     * Note: somewhat inefficient because this is called more often than
     * just when an item is updated but it works soooo
     */
    let coinTotal = 0;
    let upTotal = 0;
    const items = actorData.items;
    items.forEach(function (i) {
      if (i.type === "talent" || i.type === "spell") {
        if (i.data.upcost) {
          upTotal += i.data.upcost;
        }
      } else if (i.type === "item") {
        if (i.data.coincost && i.data.quantity) {
          coinTotal += i.data.coincost * i.data.quantity;
        }
      } else {
        console.log("why is there an item without types item, talent, spell?");
      }
    });

    data.upgradepts.spent = upTotal;
    data.coin.spent = coinTotal;
  }

}
